package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        for(Integer num: inputNumbers){
            if(num == null){
                throw new CannotBuildPyramidException();
            }
        }

        int listSize =inputNumbers.size();
        if (((Math.sqrt(1 + 8 * listSize) - 1 ) * 0.5) % 1 != 0) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int n =(int)((Math.sqrt(1 + 8 * listSize) - 1 ) * 0.5);
        int m = 2 * n - 1;
        int[][] pyramid = new int [n][m];
        int k=0;
        Iterator<Integer> iterator = inputNumbers.iterator();
        for (int i = 1; i <= n; i++){
            int pos = n - i;
            for(int j = k; j <= 2 * k; j++){
                pyramid[i - 1][pos] = iterator.next();
                pos += 2;
            }
            k++;
        }
        return pyramid;
    }


}
