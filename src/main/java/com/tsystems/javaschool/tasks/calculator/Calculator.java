package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement == null){
            return null;
        }

        List<String> listStatement = parse(statement);
        Stack<Double> values = new Stack<>();
        Stack<String> ops = new Stack<>();

        for (String str : listStatement){
            switch (str){
                case "+":
                case "-":
                case "/":
                case "*":
                    while (!ops.empty() && hasPrecedence(str,ops.peek())){
                        try {
                            values.push(applyOp(ops.pop(),values.pop(),values.pop()));
                        }catch (Exception ex){
                            return null;
                        }
                    }
                    ops.push(str);
                    break;
                case "(":
                    ops.push(str);
                    break;
                case ")":
                    while (!ops.peek().equals("(")){
                        try {
                            values.push(applyOp(ops.pop(),values.pop(),values.pop()));
                        }catch (Exception ex){
                            return null;
                        }
                    }
                    ops.pop();
                    break;
                default:
                    try {
                        Double val = Double.valueOf(str);
                        values.push(val);
                    }catch (NumberFormatException ex){
                        return null;
                    }
                    break;
            }
        }

        while (!ops.empty()){
            try {
                values.push(applyOp(ops.pop(),values.pop(),values.pop()));
            } catch (Exception ex){
                return null;
            }
        }
        if(!values.isEmpty()) {
            DecimalFormat decimalFormat = new DecimalFormat("#.####", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
            return decimalFormat.format(values.pop());
        }
        return null;
    }
    public static boolean hasPrecedence(String op1, String op2){
        if (op2.equals("(") || op2.equals(")"))
            return false;
        return (!op1.equals("*") && !op1.equals("/")) || (!op2.equals("+") && !op2.equals("-"));
    }

    private double applyOp(String op, double b, double a){
        switch (op)
        {
            case "+":
                return a + b;
            case "-":
                return a - b;
            case "*":
                return a * b;
            case "/":
                if (b == 0)
                    throw new
                            ArithmeticException();
                return a / b;
        }
        return 0;
    }

    private boolean isSymbols(char ch){
        return ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '(' || ch == ')';
    }

    private List<String> parse(String statement){
        statement = statement.trim();
        statement = statement.replaceAll("\\s+","");
        List<String> list = new ArrayList<>();
        for (int i = 0; i < statement.length(); i++) {
            char ch = statement.charAt(i);
            if(isSymbols(ch)){
                list.add(statement.substring(i, i + 1));
            }else {
                int j = i;
                while (j < statement.length() && !isSymbols(statement.charAt(j))) {
                    j++;
                }
                list.add(statement.substring(i, j));
                i = j - 1;
            }
        }
        return list;
    }
}
